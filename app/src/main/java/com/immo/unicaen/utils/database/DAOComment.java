package com.immo.unicaen.utils.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.util.Log;

import java.util.ArrayList;

/**
 * Classe manager de commentaires
 */
public class DAOComment extends DAOBase {

    public DAOComment(Context pContext) {
        super(pContext);
        this.open();
    }

    /**
     * Permet d'ajouter un commentaire
     * @param text
     *  le texte du commentaire
     * @param id_prop
     *  l'id de la propriété
     */
    public void ajouter(String text, String id_prop) {
        ContentValues values = new ContentValues();
        values.put(DatabaseHandler.COMMENT_PROPRIETE,id_prop);
        values.put(DatabaseHandler.COMMENT_TEXT,text);
        Log.d("testcomment",id_prop);
        Log.d("testcomment",text);
        Log.d("testcomment","insert");
        this.mDb.insert(DatabaseHandler.COMMENT_TABLE,null,values);
    }

    /**
     * Permet de supprimer les commentaires d'une propriété
     * @param id_prop
     *  l'id de la propriété dont on veut supprimer les commentaires
     */
    public void supprimer(String id_prop) {
        this.mDb.delete(DatabaseHandler.COMMENT_TABLE,DatabaseHandler.COMMENT_PROPRIETE+ " like ?",new String[]{id_prop});
    }

    /**
     * Permet de selectionner tous les commentaires d'une propriété
     * @param id_prop
     *  l'id de la propriété dont on veut récupérer les commentaires
     * @return
     */
    public ArrayList<String> selectAllComments(String id_prop) {
        Log.d("testcomment","execselectall");
        ArrayList<String> res = new ArrayList();
        Cursor c = mDb.rawQuery("select "+DatabaseHandler.COMMENT_TEXT+" FROM " + DatabaseHandler.COMMENT_TABLE + " WHERE "+DatabaseHandler.COMMENT_PROPRIETE+" like ?", new String[]{id_prop});
        while(c.moveToNext()) {
            String value = c.getString(0);
            Log.d("testcomment",value);
            res.add(value);
        }
        return res;
    }
}
