package com.immo.unicaen.utils.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;

import java.util.ArrayList;
import java.util.List;

/**
 * Classe abstraite representant un DAO
 */
public class DAOImage extends DAOBase {

    public DAOImage(Context pContext) {
        super(pContext);
        this.open();
    }

    /**
     * Permet d'ajouter une image
     * @param prop_id
     *  la propriete a laquelle on veut ajouter une image
     * @param url_image
     *  l'url de l'image a ajouter
     */
    public void ajouter(String prop_id,String url_image) {
        ContentValues valueCar = new ContentValues();
        valueCar.put(DatabaseHandler.IMAGE_PROPRIETE,prop_id);
        valueCar.put(DatabaseHandler.IMAGE_URL,url_image);
        this.getDb().insert(DatabaseHandler.IMAGE_TABLE,null,valueCar);
    }

    /**
     * Permet de supprimer les images d'une propriete
     * @param id_prop
     *  l'id de la porpriete dont on veut supprimer les images
     */
    public void supprimer(String id_prop) {
        this.mDb.delete(DatabaseHandler.IMAGE_TABLE,DatabaseHandler.IMAGE_PROPRIETE+" like ?",new String[] {id_prop});
    }

    /**
     * Permet de selectionner les images d'une propriete
     * @param id
     *  l'id de la proprietedont on veut recuperer les images
     * @return
     *  la liste d'image de la propriete
     */
    public ArrayList<String> selectionner(String id)
    {
        ArrayList<String> urls = new ArrayList<>();
        Cursor c = mDb.rawQuery("select "+DatabaseHandler.IMAGE_URL+" FROM " + DatabaseHandler.IMAGE_TABLE + " WHERE "+DatabaseHandler.IMAGE_PROPRIETE+" like ?", new String[]{id});
        while(c.moveToNext()) {
            String url = c.getString(0);
            urls.add(url);
        }

        return urls;
    }
}
