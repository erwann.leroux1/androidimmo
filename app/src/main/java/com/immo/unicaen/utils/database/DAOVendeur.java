package com.immo.unicaen.utils.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;

import com.immo.unicaen.model.Propriete;
import com.immo.unicaen.model.Vendeur;

import java.util.Date;

/**
 * Classe manager de vendeur
 */
public class DAOVendeur extends DAOBase {

    public DAOVendeur(Context pContext) {
        super(pContext);
        this.open();
    }

    /**
     * Methode qui va permettre d'ajouter un vendeur a la BDD si il n'existe pas
     * @param v
     *  le vendeur a ajouter
     */
    public void ajouter(Vendeur v)
    {
        // Si le vendeur n'existe pas on le créer
        if(this.selectionner(v.getId()) == null) {
            ContentValues valueVendeur = new ContentValues();
            valueVendeur.put(DatabaseHandler.VENDEUR_KEY,v.getId());
            valueVendeur.put(DatabaseHandler.VENDEUR_NOM,v.getNom());
            valueVendeur.put(DatabaseHandler.VENDEUR_PRENOM,v.getPrenom());
            valueVendeur.put(DatabaseHandler.VENDEUR_TEL,v.getTelephone());
            valueVendeur.put(DatabaseHandler.VENDEUR_MAIL,v.getEmail());
            this.getDb().insert(DatabaseHandler.VENDEUR_TABLE,null,valueVendeur);
        }
    }

    /**
     * Permet de supprimer un vendeur selon son id
     * @param id
     *  l'id du vendeur
     */
    public void supprimer(String id)
    {

        this.mDb.delete(DatabaseHandler.VENDEUR_TABLE,"= ?",new String[] {id});

    }

    /**
     * Permet de selectionner un vendeur selon son id
     * @param id
     *  l'id du vendeur
     * @return
     *  Retourne le vendeur
     */
    public Vendeur selectionner(String id)
    {

        Vendeur v = null;

        Cursor c = mDb.rawQuery("select * FROM " + DatabaseHandler.VENDEUR_TABLE + " WHERE "+DatabaseHandler.VENDEUR_KEY+" like ?", new String[]{id});
        while(c.moveToNext()) {
            String id_vendeur = c.getString(0);
            String nom= c.getString(1);
            String prenom = c.getString(2);
            String telephone = c.getString(3);
            String mail = c.getString(4);

            // On selectionne le vendeur
            v = new Vendeur(id_vendeur,nom,prenom,mail,telephone);
        }
        return v;
    }
}
