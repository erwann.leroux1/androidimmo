package com.immo.unicaen.utils.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Environment;
import android.provider.ContactsContract;
import android.util.Log;

import com.immo.unicaen.model.Propriete;
import com.immo.unicaen.model.Vendeur;

import java.io.File;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Classe manager de propriete
 */
public class DAOPropriete extends DAOBase {

    private DAOVendeur daoVendeur;

    private DAOImage daoImage;

    private DAOComment daoComment;

    private DAOCaracteristiques daoCar;

    private DAOPosseder daoPosseder;

    public DAOPropriete(Context pContext) {
        super(pContext);

        this.open();

        // Instanciation du DAO vendeur
        daoVendeur = new DAOVendeur(pContext);
        daoImage = new DAOImage(pContext);
        daoCar = new DAOCaracteristiques(pContext);
        daoPosseder = new DAOPosseder(pContext);
        daoComment = new DAOComment(pContext);
    }

    /**
     * Permet d'ajouter une propriete
     * @param p
     *  la propriete a ajouter
     */
    public void ajouter(Propriete p) {
        // Valeur de la propriete
        ContentValues valueProp = new ContentValues();
        valueProp.put(DatabaseHandler.PROPRIETE_KEY, p.getId());
        valueProp.put(DatabaseHandler.PROPRIETE_TITRE, p.getTitre());
        valueProp.put(DatabaseHandler.PROPRIETE_DESCRIPTION, p.getDescription());
        valueProp.put(DatabaseHandler.PROPRIETE_PIECES, p.getPieces());
        valueProp.put(DatabaseHandler.PROPRIETE_PRIX, p.getPrix());
        valueProp.put(DatabaseHandler.PROPRIETE_VILLE, p.getVille());
        valueProp.put(DatabaseHandler.PROPRIETE_CP, p.getCp());
        valueProp.put(DatabaseHandler.PROPRIETE_VENDEUR, p.getVendeur().getId());
        valueProp.put(DatabaseHandler.PROPRIETE_DATE, p.getDate());


        // Ajout des images
        List<String> imgs_url = p.getImages();
        for(String i : imgs_url) {
            daoImage.ajouter(p.getId(),i);
        }

        // Insertion dans la table propriete
        this.getDb().insert(DatabaseHandler.PROPRIETE_TABLE, null, valueProp);

        //Insertion du vendeur
        daoVendeur.ajouter(p.getVendeur());

        // Ajout des caracteristiques
        List<String> caracteristiques = p.getCaracteristiques();
        for(String c : caracteristiques) {
            daoCar.ajouter(c);
            // Ajout des liens entre proprietes et caracteristiques
            daoPosseder.ajouter(p.getId(),c);
        }


        // Insertion des caracteristiques
    }

    /**
     * Peremt de supprimer une propriete
     * @param id l'identifiant de la propiete à supprimer
     */
    public void supprimer(String id) {
        this.daoPosseder.supprimer(id);
        this.daoImage.supprimer(id);
        this.daoComment.supprimer(id);
        this.mDb.delete(DatabaseHandler.PROPRIETE_TABLE,DatabaseHandler.PROPRIETE_KEY+" = ?",new String[] {id});

        //On va egalement supprimer les images
        File storageDir =  context.getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        for(File f : storageDir.listFiles()) {
            if(f.getName().contains(id)) {
                f.delete();
            }
        }
    }



    /**
     * Peremet de selectionner une proprite
     * @param id l'identifiant de la propiete à récupérer
     */
    public Propriete selectionner(String id) {

        Propriete p = null;

        Cursor c = mDb.rawQuery("select * FROM " + DatabaseHandler.PROPRIETE_TABLE + " WHERE "+DatabaseHandler.PROPRIETE_KEY+" like ?", new String[]{id});
        while(c.moveToNext()) {
            String id_prop = c.getString(0);
            String titre= c.getString(1);
            int pieces = c.getInt(2);
            int prix = c.getInt(3);
            String ville = c.getString(4);
            String cp = c.getString(5);
            String id_vendeur = c.getString(6);
            String desc = c.getString(7);
            int time = c.getInt(8);

            // On selectionne le vendeur
            Vendeur v = daoVendeur.selectionner(id_vendeur);

            ArrayList<String> imgs = daoImage.selectionner(id_prop);

            ArrayList<String> caract = daoPosseder.selectionnerPropCat(id_prop);

            // On créer la propriete
            p = new Propriete(id_prop,titre,desc,pieces,caract,prix,ville,cp,v,imgs,time);
        }
        return p;
    }


    /**
     * Permet deselectionner toutes les proprietes
     * @return
     *  la liste de proprietes
     */
    public List<Propriete> selectionnerTout()
    {

        ArrayList<Propriete> proprietes = new ArrayList();

        Cursor c = mDb.rawQuery("select * FROM " + DatabaseHandler.PROPRIETE_TABLE, null);

        while(c.moveToNext()) {
            proprietes.add(this.selectionner(c.getString(0)));
        }

        return proprietes;
    }

}