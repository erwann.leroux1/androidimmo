package com.immo.unicaen.utils.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

/**
 * Classe manager de caracteristiques
 */
public class DAOCaracteristiques extends DAOBase {

    public DAOCaracteristiques(Context pContext) {
        super(pContext);
        this.open();
    }

    /**
     * Permet d'ajouter une caracteristique
     * @param libelle_car
     *  le libelle de la caracteristique a ajouter
     */
    public void ajouter(String libelle_car) {
        ContentValues valueCar = new ContentValues();
        valueCar.put(DatabaseHandler.CAR_LIBELLE,libelle_car);
        boolean exist = this.libelleExists(libelle_car);
        // Si la caracteristique n'existe pas on l'ajoute
        if(!exist) {
            this.getDb().insert(DatabaseHandler.CARACTERISTIQUES_TABLE,null,valueCar);
        }
    }

    /**
     * Permet de supprimer une caracteristique
     * @param libelle_car
     *  le libelle de la caracteristique a supprimer
     */
    public void supprimer(String libelle_car) {
        this.mDb.delete(DatabaseHandler.CARACTERISTIQUES_TABLE,"libelle_car = ?",new String[] {libelle_car});
    }

    /**
     * Permet de savoir si la caracteristique existe deja
     * @param libelle
     *  le libelle de la caracteristique que l'on recherce
     * @return
     */
    public boolean libelleExists(String libelle) {
        Cursor c = mDb.rawQuery("select * FROM " + DatabaseHandler.CARACTERISTIQUES_TABLE+" where "+DatabaseHandler.CAR_LIBELLE+" like ?", new String[]{libelle});
        boolean exist = c.moveToFirst();
        return exist;
    }

    /**
     * Permet de selectionner le libelle en fonction de l'id de la caracteristique
     * @param id_car
     *  l'id de la caracteristique
     * @return
     *  Retourne le libelle de la caracteristique
     */
    public String selectionnerLibelle(int id_car) {
        Cursor c = mDb.rawQuery("select * FROM " + DatabaseHandler.CARACTERISTIQUES_TABLE+" where "+DatabaseHandler.CAR_KEY+" like ?", new String[]{""+id_car});
        c.moveToNext();
        String value = c.getString(1);
        return value;
    }

    /**
     * Permet de selectionner l'id en fonction du libelle de la caracteristique
     * @param libelle
     *  le libelle de la caracteristique
     * @return
     *  Retourne l'id de la caracteristique
     */
    public Integer selectionnerId(String libelle) {
        Log.d("caract","Lib : " + libelle);
        Cursor c = mDb.rawQuery("select "+DatabaseHandler.POSSEDER_KEY2+" FROM " + DatabaseHandler.CARACTERISTIQUES_TABLE+" where "+DatabaseHandler.CAR_LIBELLE+" like ?", new String[]{libelle});
        c.moveToNext();
        int value = c.getInt(0);
        Log.d("caract","value : " + value);
        return value;
    }

}
