package com.immo.unicaen.utils.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;

/**
 * Classe manager de la base de donnees
 */
public class DatabaseHandler extends SQLiteOpenHelper {

    public static final String PROPRIETE_TABLE = "propriete";

    // Champs de la table propriete
    public static final String PROPRIETE_KEY = "id_prop";
    public static final String PROPRIETE_TITRE = "titre";
    public static final String PROPRIETE_DESCRIPTION = "description";
    public static final String PROPRIETE_PIECES = "pieces";
    public static final String PROPRIETE_PRIX = "prix";
    public static final String PROPRIETE_VILLE = "ville";
    public static final String PROPRIETE_CP = "code_postal";
    public static final String PROPRIETE_VENDEUR = "id_vendeur";
    public static final String PROPRIETE_DATE = "date";

    public static final String VENDEUR_TABLE = "vendeur";
    // Champs de la table vendeur
    public static final String VENDEUR_KEY = "id_vendeur";
    public static final String VENDEUR_NOM = "nom";
    public static final String VENDEUR_PRENOM= "prenom";
    public static final String VENDEUR_MAIL = "email";
    public static final String VENDEUR_TEL = "telephone";

    public static final String CARACTERISTIQUES_TABLE = "caracteristiques";
    // Champs de la table caracteristiques
    public static final String CAR_KEY = "id_car";
    public static final String CAR_LIBELLE = "libelle";

    public static final String POSSEDER_TABLE = "posseder";
    // Champs de la table posseder qui associe une caracteristique a une propriete
    public static final String POSSEDER_KEY1 = "id_prop";
    public static final String POSSEDER_KEY2 = "id_car";

    public static final String IMAGE_TABLE = "images";

    public static final String IMAGE_KEY = "images";
    public static final String IMAGE_PROPRIETE = "prop_id";
    public static final String IMAGE_URL = "url";

    public static final String COMMENT_TABLE = "comment";

    public static final String COMMENT_KEY = "id_comment";
    public static final String COMMENT_PROPRIETE = "comment_prop_id";
    public static final String COMMENT_TEXT = "comment_text";

    //DROP des tables
    public static final String PROPRIETE_TABLE_DROP = "DROP TABLE IF EXISTS " + PROPRIETE_TABLE + ";";
    public static final String VENDEUR_TABLE_DROP = "DROP TABLE IF EXISTS " + VENDEUR_TABLE + ";";
    public static final String CARACTERISTIQUES_TABLE_DROP = "DROP TABLE IF EXISTS " + CARACTERISTIQUES_TABLE + ";";
    public static final String POSSEDER_TABLE_DROP = "DROP TABLE IF EXISTS " + POSSEDER_TABLE + ";";
    public static final String IMAGE_TABLE_DROP = "DROP TABLE IF EXISTS " + IMAGE_TABLE + ";";
    public static final String COMMENT_TABLE_DROP = "DROP TABLE IF EXISTS " + COMMENT_TABLE + ";";

    public final String caracteristiques[] = new String[]{"piscine","garage","jardin","balcon","sauna"};

    public static final String PROPRIETE_TABLE_CREATE =

            "CREATE TABLE " + PROPRIETE_TABLE + " (" +

                    PROPRIETE_KEY + " TEXT PRIMARY KEY, " +

                    PROPRIETE_TITRE + " TEXT, " +

                    PROPRIETE_PIECES + " INTEGER, " +

                    PROPRIETE_PRIX + " INT, " +

                    PROPRIETE_VILLE + " TEXT, " +

                    PROPRIETE_CP + " TEXT, " +

                    PROPRIETE_VENDEUR + " TEXT," +

                    PROPRIETE_DESCRIPTION + " TEXT, " +

                    PROPRIETE_DATE + " INT, " +

                    "FOREIGN KEY("+PROPRIETE_VENDEUR+") REFERENCES "+VENDEUR_TABLE+"("+VENDEUR_KEY+"))";


    public static final String VENDEUR_TABLE_CREATE =

            "CREATE TABLE " + VENDEUR_TABLE + " (" +

                    VENDEUR_KEY + " TEXT PRIMARY KEY, " +

                    VENDEUR_NOM + " TEXT, " +

                    VENDEUR_PRENOM + " TEXT, " +

                    VENDEUR_MAIL + " TEXT, " +

                    VENDEUR_TEL + " TEXT);";


    public static final String CARACTERISTIQUES_TABLE_CREATE =

            "CREATE TABLE " + CARACTERISTIQUES_TABLE + " (" +

                    CAR_KEY + " INTEGER PRIMARY KEY AUTOINCREMENT, " +

                    CAR_LIBELLE + " TEXT);";


    public static final String POSSEDER_TABLE_CREATE =

            "CREATE TABLE " + POSSEDER_TABLE + " (" +

                    POSSEDER_KEY1+ " TEXT," +

                    POSSEDER_KEY2 + " TEXT,"+

        "PRIMARY KEY("+POSSEDER_KEY1+","+POSSEDER_KEY2+"));";


    public static final String IMAGE_TABLE_CREATE =

            "CREATE TABLE " + IMAGE_TABLE + " (" +

                    IMAGE_KEY + " INTEGER PRIMARY KEY AUTOINCREMENT, " +

                    IMAGE_URL + " TEXT, " +

                    IMAGE_PROPRIETE+ " TEXT, "+

                    "FOREIGN KEY("+IMAGE_PROPRIETE+") REFERENCES "+PROPRIETE_TABLE+"("+PROPRIETE_KEY+"))";


    public static final String COMMENT_TABLE_CREATE =

            "CREATE TABLE "+COMMENT_TABLE+ " (" +

                    COMMENT_KEY+ " INTEGER PRIMARY KEY AUTOINCREMENT, " +

                    COMMENT_PROPRIETE + " TEXT, " +

                    COMMENT_TEXT + " TEXT, " +

                    "FOREIGN KEY("+COMMENT_PROPRIETE+") REFERENCES "+PROPRIETE_TABLE+"("+PROPRIETE_KEY+"))";


    public DatabaseHandler(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
    }


    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(PROPRIETE_TABLE_CREATE);
        db.execSQL(POSSEDER_TABLE_CREATE);
        db.execSQL(CARACTERISTIQUES_TABLE_CREATE);
        db.execSQL(VENDEUR_TABLE_CREATE);
        db.execSQL(IMAGE_TABLE_CREATE);
        db.execSQL(COMMENT_TABLE_CREATE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int i, int i1) {
        db.execSQL(POSSEDER_TABLE_DROP);
        db.execSQL(CARACTERISTIQUES_TABLE_DROP);
        db.execSQL(VENDEUR_TABLE_DROP);
        db.execSQL(PROPRIETE_TABLE_DROP);
        db.execSQL(IMAGE_TABLE_DROP);
        db.execSQL(COMMENT_TABLE_DROP);
    }


}