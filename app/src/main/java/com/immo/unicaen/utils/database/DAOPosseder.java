package com.immo.unicaen.utils.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.util.Log;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;

/**
 * Classe manager de posseder
 */
public class DAOPosseder extends DAOBase {

    private DAOCaracteristiques daoCar;

    public DAOPosseder(Context pContext) {
        super(pContext);
        daoCar = new DAOCaracteristiques(pContext);
        this.open();
    }

    /**
     * Permet d'ajouter un lien entre une propriete et une caracteristique
     * @param id_prop
     *  l'id de la propriete a laquelle on veut ajouter une caracteristique
     * @param libelle_cat
     *  le libelle de la categorie
     */
    public void ajouter(String id_prop,String libelle_cat) {
        ContentValues valueCar = new ContentValues();
        valueCar.put(DatabaseHandler.POSSEDER_KEY1,id_prop);
        daoCar.ajouter(libelle_cat);
        valueCar.put(DatabaseHandler.POSSEDER_KEY2,daoCar.selectionnerId(libelle_cat));
        this.getDb().insert(DatabaseHandler.POSSEDER_TABLE,null,valueCar);
    }

    /**
     * Permet de supprimer les caracteristiques d'une propriete
     * @param id_prop
     *  l'id de la propriete
     */
    public void supprimer(String id_prop) {
        this.mDb.delete(DatabaseHandler.POSSEDER_TABLE,DatabaseHandler.POSSEDER_KEY1+" like ?",new String[] {id_prop});
    }

    /**
     * Permet de selectionner toutes les caracteristiques d'une propriete
     * @param id_prop
     *  l'id de la propriete dont on veut recuperer les caracteristiques
     * @return
     */
    public ArrayList<String> selectionnerPropCat(String id_prop)
    {
        ArrayList<Integer> carId = new ArrayList();
        Cursor c = mDb.rawQuery("select "+DatabaseHandler.POSSEDER_KEY2+" FROM " + DatabaseHandler.POSSEDER_TABLE + " WHERE "+DatabaseHandler.POSSEDER_KEY1+" like ?", new String[]{id_prop});
        while(c.moveToNext())
        {
            carId.add(c.getInt(0));
        }
        ArrayList<String> carLibelle = new ArrayList();
        for(Integer id : carId)
        {
           carLibelle.add(daoCar.selectionnerLibelle(id));
        }
        return carLibelle;
    }
}
