package com.immo.unicaen.adapters;

import android.util.Log;

import com.immo.unicaen.model.Propriete;
import com.squareup.moshi.FromJson;
import com.squareup.moshi.JsonAdapter;
import com.squareup.moshi.JsonReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * JSONAdapter de propriete pour moshi
 */
public class ProprieteJSONAdapter  {
    @FromJson
    List<Propriete> fromJson(JsonReader reader, JsonAdapter<Propriete> delegate) throws IOException {

        List<Propriete> proprietes = new ArrayList();

        reader.beginObject();
        if(reader.hasNext())
        {
            while (reader.hasNext()) {
                String name = reader.nextName();
                if (name.equals("success")) {
                    boolean success = reader.nextBoolean();
                    Log.i("JML", "Success vaut " + success);
                    if (!success) {
                        // @todo : récupérer le message d'erreur et le donner à l'exception
                        // @todo : créer une exception spécifique pour la distinguer des IOException
                        throw new IOException("API a répondu FALSE");
                    }
                } else if (name.equals("response")) {
                    // déléguer l'extraction à l'adapteur qui transforme du Json en Personne
                    reader.beginArray();
                    while(reader.hasNext()) {
                        Propriete p;
                        p = delegate.fromJson(reader);
                        Log.d("propsize",""+p.getImages().size());
                        proprietes.add(p);
                    }
                    reader.endArray();
                } else {
                    // dans notre cas on ne devrait pas avoir d'autres clés que success et response dans le Json
                    throw new IOException("Response contient des données non conformes");
                }
            }
        }
        reader.endObject();
        return proprietes;
    }
}
