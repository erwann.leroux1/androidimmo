package com.immo.unicaen.adapters;

import android.content.Context;
import android.databinding.BindingAdapter;
import android.text.format.DateFormat;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.immo.unicaen.activity.R;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Classe qui permet de customiser les affichages fait grace au data binding
 */
public class CustomBindingAdapters {

    /**
     * Affichage des images
      * @param view
     *  l'imageView dans laquelle on veut afficher l'image
     * @param imageUrl
     *  la liste d'image qu'on veut afficher
     */
    @BindingAdapter("imageUrl")
    public static void setImageResources(ImageView view, ArrayList<String> imageUrl) {
        Context context = view.getContext();

        RequestOptions option = new RequestOptions()
                .placeholder(R.drawable.ic_launcher_background)
                .error(R.drawable.ic_launcher_background);

        Glide.with(context)
                .setDefaultRequestOptions(option)
                .load(imageUrl.get(0))
                .into(view);
    }

    /**
     * Affichage des caracteristiques
     * @param t
     *  le textview dans lequel on veut afficher l'image
     * @param c
     *  la liste de caracteristiques que l'on veut afficher
     */
    @BindingAdapter("caracteristiques")
    public static void displayCaract(TextView t, List<String> c) {
        String res = "";
        for(String caract : c) {
            res += "- "+caract+"\n";
        }
        t.setText(res);
    }

    /**
     * Affichage de la date
     * @param t
     * le textview dans lequel on veut afficher la date
     * @param date
     * la valeur de la date en seconde que l'on veut afficher
     */
    @BindingAdapter("date")
    public static void formateDate(TextView t, int date) {
        Date d = new Date(date);
        String dayOfTheWeek = (String) DateFormat.format("EEEE", d); // Thursday
        String day          = (String) DateFormat.format("dd",   d); // 20
        String monthString  = (String) DateFormat.format("MMM",  d); // Jun
        String year         = (String) DateFormat.format("yyyy", d); // 2013
        t.setText("Publié le "+dayOfTheWeek+" "+day+" "+monthString+" "+year);
    }

}
