package com.immo.unicaen.adapters;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.immo.unicaen.activity.DetailActivity;
import com.immo.unicaen.activity.R;
import com.immo.unicaen.model.Propriete;
import com.immo.unicaen.utils.database.DAOPropriete;

import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Classe qui va permettre de creer le recycler view
 */
public class RecyclerViewAdapter extends RecyclerView.Adapter<RecyclerViewAdapter.ViewHolder> {

    private static final String TAG = "RecyclerViewAdapter";

    private String flag = "";

    private List<Propriete> proprietes = null;
    private Context mContext;

    /**
     * Constructeur du recyclerView
     * @param mPropriete
     *  la liste de proprietes
     * @param mContext
     *  le context
     * @param flag_src
     *  flag qui permet de se siter ( local ou non )
     */
    public RecyclerViewAdapter(List<Propriete> mPropriete, Context mContext,String flag_src) {
        this.proprietes = mPropriete;
        this.mContext = mContext;
        this.flag = flag_src;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_listeitem,parent, false);
        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        Glide.with(mContext)
                .asBitmap()
                .load(proprietes.get(position).getImages().get(0))
                .into(holder.image);

        holder.imageName.setText(proprietes.get(position).getTitre());

        holder.parentLayout.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                Context context = view.getContext();
                Intent intent = new Intent(context, DetailActivity.class);
                intent.putExtra("from",flag);
                intent.putExtra("propriete",proprietes.get(position));
                context.startActivity(intent);
            }
        });

        // Definition des actions sur clic long
        holder.parentLayout.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(final View view) {
                final AlertDialog.Builder builder = new AlertDialog.Builder(view.getContext());

                // Preparation de l'acces aux donnees
                final DAOPropriete propDAO = new DAOPropriete(view.getContext());
                propDAO.open();
                // On verifie que la propriete n'est pas deja dans la base
                Propriete p = propDAO.selectionner(proprietes.get(position).getId());
                if(p == null) {
                    builder.setMessage("Ajouter aux favoris")
                            .setPositiveButton("Oui", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                   propDAO.ajouter(proprietes.get(position));
                                   Toast toast = Toast.makeText(view.getContext(), "Annonce sauvegardée", Toast.LENGTH_SHORT);

                                   toast.show();
                                }
                            })
                            .setNegativeButton("Non", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    // On ne fait rien
                                }
                            });
                } else {
                    builder.setMessage("Déja enregistrée dans les favoris. Supprimer ?")
                            .setPositiveButton("Oui", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    propDAO.supprimer(proprietes.get(position).getId());
                                    proprietes.remove(position);
                                    holder.parentLayout.removeViewAt(position);
                                    notifyItemRemoved(position);
                                    notifyItemRangeChanged(position, proprietes.size());
                                    Toast toast = Toast.makeText(view.getContext(), "Annonce supprimée", Toast.LENGTH_SHORT);
                                    toast.show();
                                }
                            })
                            .setNegativeButton("Non", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    // User cancelled the dialog
                                }
                            });
                }
                builder.show();
                return true;
            }
        });
    }

    @Override
    public int getItemCount() {
        return proprietes.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        CircleImageView image;
        TextView imageName;
        RelativeLayout parentLayout;

        public ViewHolder(View itemView) {
            super(itemView);
            image = itemView.findViewById(R.id.image);
            imageName= itemView.findViewById(R.id.image_name);
            parentLayout= itemView.findViewById(R.id.parent_layout);
        }


    }
}

