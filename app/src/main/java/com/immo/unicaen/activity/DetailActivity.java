package com.immo.unicaen.activity;

import android.app.Activity;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.content.FileProvider;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.immo.unicaen.activity.databinding.ActivityHasardBinding;
import com.immo.unicaen.adapters.ViewPagerAdapter;
import com.immo.unicaen.model.Propriete;
import com.immo.unicaen.model.Vendeur;
import com.immo.unicaen.utils.database.DAOComment;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

/**
 * Activity qui va afficher toutes les informations d'une propriété
 */
public class DetailActivity extends Activity {

    private Propriete p;

    private DAOComment daoComment;

    private ListView lv;

    private Button commenter;

    private EditText textArea;

    private String currentPhotoPath;

    private File storageDir;

    private String from_flag = "";

    private ArrayList<String> comments;

    static final int REQUEST_TAKE_PHOTO = 1;

    private ViewPager viewPager;

    private ViewPagerAdapter vpAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_hasard);

        // Recuperation du flag source
        from_flag = this.getIntent().getStringExtra("from");

        this.daoComment = new DAOComment(this);

        this.lv = findViewById(R.id.commentlist);

        this.commenter = findViewById(R.id.btncommenter);

        this.storageDir = getExternalFilesDir(Environment.DIRECTORY_PICTURES);

        Intent myIntent = getIntent(); // gets the previously created intent
        p = (Propriete) myIntent.getSerializableExtra("propriete");

        // On verifie si on a une propriete en parametre
        if(myIntent.hasExtra("propriete")) {
            this.p = (Propriete) myIntent.getSerializableExtra("propriete");
            this.comments = daoComment.selectAllComments(p.getId());
            if(from_flag.equals("local")) {
                this.setTitle("Annonce de " + p.getVendeur().getPrenom());
                for (File f : storageDir.listFiles()) {
                    if (f.getName().contains(this.p.getId())) {
                        this.p.getImages().add(f.getAbsolutePath());
                    }
                }
            }
        }

        this.textArea = (EditText) findViewById(R.id.commentaires);

        final ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(
                this,
                android.R.layout.simple_list_item_1,
                comments);


        /* Binding de la vue */
        final ActivityHasardBinding binding = DataBindingUtil.setContentView(this, R.layout.activity_hasard);
        binding.setProp(this.p);

        viewPager = findViewById(R.id.viewpager);

        this.vpAdapter = new ViewPagerAdapter(this,p.getImages());

        viewPager.setAdapter(vpAdapter);

        binding.commentlist.setAdapter(arrayAdapter);

        binding.btncommenter.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                daoComment.ajouter(binding.commentaires.getText().toString(),p.getId());
                comments.add(binding.commentaires.getText().toString());
                binding.commentaires.setText("");
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        arrayAdapter.notifyDataSetChanged();
                    }
                });
            }
        });

        View.OnTouchListener listener  = new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                view.getParent().requestDisallowInterceptTouchEvent(true);
                switch (motionEvent.getAction() & MotionEvent.ACTION_MASK){
                    case MotionEvent.ACTION_UP:
                        view.getParent().requestDisallowInterceptTouchEvent(false);
                        break;
                }
                return false;
            }
        };

        binding.takepicture.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dispatchTakePictureIntent();
            }
        });

        binding.commentlist.setOnTouchListener(listener);
        binding.commentaires.setOnTouchListener(listener);

        /*
         * On cache les elements qu'on ne veut pas voir quand on est pas en local
         */
        if(!from_flag.equals("local")) {
            binding.commentlist.setVisibility(View.GONE);
            binding.commentaires.setVisibility(View.GONE);
            binding.btncommenter.setVisibility(View.GONE);
            binding.takepicture.setVisibility(View.GONE);
            binding.labelcommentaires.setVisibility(View.GONE);
        }

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        // Check which request we're responding to
        if (requestCode == 1) {
            // Make sure the request was successful
            if (resultCode == RESULT_OK) {
                // NO NEED
            }
        }
    }

    /**
     * Fonction qui va créer le fichier image
     * @return
     *  le fichier image
     * @throws IOException
     */
    private File createImageFile() throws IOException {
        // Create an image file name
        String imageFileName = this.p.getId()+"_";
        File image = File.createTempFile(
                imageFileName,  /* prefix */
                ".jpg",         /* suffix */
                storageDir      /* directory */
        );

        // Save a file: path for use with ACTION_VIEW intents
        currentPhotoPath = image.getAbsolutePath();
        return image;
    }

    /**
     * Fonction qui va se charger de prendre la photo
     */
    private void dispatchTakePictureIntent() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        // Ensure that there's a camera activity to handle the intent
        if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
            // Create the File where the photo should go
            File photoFile = null;
            try {
                photoFile = createImageFile();
            } catch (IOException ex) {
                // Error occurred while creating the File
            }
            // Continue only if the File was successfully created
            if (photoFile != null) {

                Uri photoURI = FileProvider.getUriForFile(this,
                        "com.immo.unicaen.fileprovider",
                        photoFile);

                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                startActivityForResult(takePictureIntent, REQUEST_TAKE_PHOTO);
            }
            this.p.getImages().add(photoFile.getAbsolutePath());
            vpAdapter.notifyDataSetChanged();
        }
    }


    /**
     * Gestion de l'action a réaliser selon l'item du menu qui a été sélectionné
     * @param item
     *  l'item du menu séléctionné
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                if(this.getIntent().hasExtra("from")) {
                    if(from_flag.equals("local")) {
                        Intent intent = new Intent(this, ListeActivity.class);
                        intent.putExtra("local","true");
                        this.startActivity(intent);
                    } else if(from_flag.equals("liste")) {
                        Intent intent = new Intent(this, ListeActivity.class);
                        intent.putExtra("liste","true");
                        this.startActivity(intent);
                    } else {
                        Intent intent = new Intent(this, ListeActivity.class);
                        this.startActivity(intent);
                    }
                }
                break;
        }
        return true;
    }

}

