package com.immo.unicaen.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;

import com.immo.unicaen.adapters.ProprieteJSONAdapter;
import com.immo.unicaen.adapters.RecyclerViewAdapter;
import com.immo.unicaen.adapters.ViewPagerAdapter;
import com.immo.unicaen.model.Propriete;
import com.immo.unicaen.utils.database.DAOPropriete;
import com.squareup.moshi.JsonAdapter;
import com.squareup.moshi.Moshi;
import com.squareup.moshi.Types;
import com.squareup.okhttp.Callback;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

/**
 * Activity qui va permettre d'afficher les differentes listes
 */
public class ListeActivity extends Activity {

    private static final String TAG = "ListeActivity";

    private ArrayList<String> mNames = new ArrayList();
    private ArrayList<String> mThumbails = new ArrayList();
    private List<Propriete> proprietes = new ArrayList();

    private Menu menu;
    private String flag = "";

    @Override
    protected void onCreate(Bundle savedInstance) {
        super.onCreate(savedInstance);
        setContentView(R.layout.activity_liste);

        if(this.getIntent().hasExtra("local")) {
            // On veut afficher la liste local
            this.setTitle("Annonces favorites");
            DAOPropriete daoPropriete = new DAOPropriete(this);
            proprietes = (ArrayList<Propriete>) daoPropriete.selectionnerTout();
            this.flag = "local";
            initRecyclerView();
        } else if(this.getIntent().hasExtra("liste")) {
            // On va charger les dernieres annonce donc avec une api differente
            this.setTitle("Liste des annonces");
            initListItems("https://ensweb.users.info.unicaen.fr/android-estate/mock-api/liste.json");
            this.flag = "liste";
        }  else {
            // On charge les annonces de la mock API
            this.setTitle("Accueil - Dernières Annonces");
            initListItems("https://ensweb.users.info.unicaen.fr/android-estate/mock-api/dernieres.json");
        }
    }

    /**
     * Permet d'initialiser la liste
     * @param apiUrl
     *  l'url de l'api a utiliser selon la liste que l'on veut
     */
    private void initListItems(String apiUrl) {

        OkHttpClient okHttpClient = new OkHttpClient();
        Request myGetRequest = new Request.Builder()
                .url(apiUrl)
                .build();

        okHttpClient.newCall(myGetRequest).enqueue(new Callback() {
            @Override
            public void onFailure(Request request, IOException e) {
                Log.d("okhttp","FAILURE");
            }

            @Override
            public void onResponse(Response response) throws IOException {
                //instanciation de Moshi
                Moshi moshi = new Moshi.Builder().add(new ProprieteJSONAdapter()).build();

                Type type = Types.newParameterizedType(List.class, Propriete.class);
                JsonAdapter<List<Propriete>> adapter = moshi.adapter(type);

                try {
                    // response est la String qui contient le JSON de la réponse
                    proprietes = adapter.fromJson(response.body().string());
                    // ... faire ce qu'il faut ensuite
                } catch (IOException e) {
                    Log.i("JML", "Erreur I/O");
                }

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        initRecyclerView();
                    }
                });
            }
        });
    }

    /**
     * Permet d'initialiser le recyclerView
     */
    private void initRecyclerView()
    {
        for(Propriete p : proprietes) {
            this.mNames.add(p.getTitre());
            this.mThumbails.add(p.getImages().get(0));
        }
        RecyclerView recyclerView = findViewById(R.id.recyclerv_view);
        RecyclerViewAdapter adapter = new RecyclerViewAdapter(proprietes,this,flag);
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
    }

    /**
     * Creation du menu
     * @param menu
     * @return
     */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu, menu);
        this.menu = menu;
        return true;
    }

    /**
     * Gestion de l'action a réaliser selon l'item du menu qui a été sélectionné
     * @param item
     *  l'item du menu séléctionné
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.liste:
                Intent listeIntent = new Intent(ListeActivity.this,ListeActivity.class);
                listeIntent.putExtra("liste","true");
                ListeActivity.this.startActivity(listeIntent);
                break;
            case R.id.local:
                Intent localListIntent = new Intent(ListeActivity.this, ListeActivity.class);
                localListIntent.putExtra("local","true");
                ListeActivity.this.startActivity(localListIntent);
                break;
            case R.id.accueil:
                Intent accueilListIntent = new Intent(ListeActivity.this, ListeActivity.class);
                ListeActivity.this.startActivity(accueilListIntent);
                break;
        }
        return super.onOptionsItemSelected(item);
    }
}