package com.immo.unicaen.model;


import android.os.Parcel;
import android.os.Parcelable;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Classe representant une propriete
 */
public class Propriete implements Serializable {

    private String id;

    private String titre;

    private String description;

    private int pieces;

    private List<String> caracteristiques;

    private int prix;

    private String ville;

    private String cp;

    private Vendeur vendeur;

    private List<String> images;

    private int date;

    public Propriete(){
        this.caracteristiques = new ArrayList();
        this.images = new ArrayList();
    }

    public Propriete(String pId, String pTitre, String pDesc, int pPieces,
                     ArrayList<String> pCar,int pPrix,String pVille,
                     String pCp, Vendeur pVendeur, ArrayList<String> pUrl,
                     int pDate) {

        this.setId(pId);

        this.setTitre(pTitre);

        this.setDescription(pDesc);

        this.setPieces(pPieces);

        this.setCaracteristiques(pCar);

        this.setPrix(pPrix);

        this.setVille(pVille);

        this.setCp(pCp);

        this.setVendeur(pVendeur);

        this.setImages(pUrl);

        this.setDate(pDate);
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitre() {
        return titre;
    }

    public void setTitre(String titre) {
        this.titre = titre;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getPieces() {
        return pieces;
    }

    public void setPieces(int pieces) {
        this.pieces = pieces;
    }

    public List<String> getCaracteristiques() {
        return caracteristiques;
    }

    public void setCaracteristiques(List<String> caracteristiques) {
        this.caracteristiques = caracteristiques;
    }

    public int getPrix() {
        return prix;
    }

    public void setPrix(int prix) {
        this.prix = prix;
    }

    public String getVille() {
        return ville;
    }

    public void setVille(String ville) {
        this.ville = ville;
    }

    public String getCp() {
        return cp;
    }

    public void setCp(String cp) {
        this.cp = cp;
    }

    public Vendeur getVendeur() {
        return vendeur;
    }

    public void setVendeur(Vendeur vendeur) {
        this.vendeur = vendeur;
    }

    public List<String> getImages() {
        return images;
    }

    public void setImages(ArrayList<String> imgsURL) {
        this.images = imgsURL;
    }

    public int getDate() { return date; }

    public void setDate(int date) {
        this.date = date;
    }

    @Override
    public String toString()
    {
        return "Propriete : "+this.getId();
    }
}
