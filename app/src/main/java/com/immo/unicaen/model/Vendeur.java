package com.immo.unicaen.model;

import java.io.Serializable;

/**
 * Classe représentant un vendeur
 */
public class Vendeur implements Serializable {

    private String id;

    private String nom;

    private String prenom;

    private String email;

    private String telephone;

    public Vendeur()
    {

    }

    public Vendeur(String pId, String pNom, String pPrenom, String pEmail, String pTelephone)
    {
        this.setId(pId);

        this.setNom(pNom);

        this.setPrenom(pPrenom);

        this.setEmail(pEmail);

        this.setTelephone(pTelephone);
    }


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }


}
